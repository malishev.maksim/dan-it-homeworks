import React from 'react';
import { combineReducers } from "redux"
import itemReducer from "./item-reducer"
import favouritesReducer from './favourites-reducer';
import itemsInCartReducer from './item-in-cart-reducer';
import modalStateReducer from './modal-state-reducer';
import { reducer as formReducer} from "redux-form";
import showFormReducer from './show-form-reducer';

export default combineReducers({
  itemReducer,
  favouritesReducer,
  itemsInCartReducer,
  modalStateReducer,
  showFormReducer,
  form: formReducer
}); 