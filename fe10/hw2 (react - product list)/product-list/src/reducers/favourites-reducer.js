import React from 'react';
import * as types from "../constants/types"

const initState = {
  favouriteItems: []
}

export default function favouritesReducer(state = initState, action = {}) {
  switch (action.type) {
    case types.TOGGLE_FAVOURITE: {

      let foundItem;
      let { favouriteItems } = state;
      let item = action.payload;
      if (!favouriteItems) {
        favouriteItems = [item]
      } else {
        foundItem = favouriteItems.find(curItem => curItem.sku === item.sku);
        if (foundItem) {
          favouriteItems = favouriteItems.filter(item => item !== foundItem)
        } else {
          favouriteItems = [...favouriteItems, item]
        }
      }

      return ({
        ...state,
        favouriteItems
      })
    }
    default: {
      return state;
    }
  }
}