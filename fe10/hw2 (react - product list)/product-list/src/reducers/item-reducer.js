import React from 'react';
import * as types from "../constants/types"

const initState = {
  items: []
}

export default function itemsReducer(state = initState, action = {}) {
  switch (action.type) {
    case types.ADD_ITEMS: {
      return ({
        ...state,
        items: action.payload
      })
    }
    default : {
      return state;
    }
  }
}