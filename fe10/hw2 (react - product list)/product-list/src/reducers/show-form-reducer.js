import React from 'react';
import * as types from "../constants/types"

const initState = {
  isCustomerFormOpen: false
}

export default function showFormReducer(state = initState, action = {}) {
  switch (action.type) {
    case types.TOGGLE_FORM: {
      return ({
        ...state,
        isCustomerFormOpen: !state.isCustomerFormOpen,
      })
    }
    default: {
      return state;
    }
  }
}