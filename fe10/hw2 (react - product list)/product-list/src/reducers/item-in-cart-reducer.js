import React from 'react';
import * as types from "../constants/types"

const initState = {
  itemsInCart: []
}

export default function itemsInCartReducer(state = initState, action = {}) {
  switch (action.type) {
    case types.TOGGLE_ITEM_IN_CART: {

      let foundItem;
      let { itemsInCart } = state;
      let item = action.payload;
      if (itemsInCart.length == 0) {
        itemsInCart = [item]
      } else {
        foundItem = itemsInCart.find(curItem => curItem.sku === item.sku);
        if (foundItem) {
          itemsInCart = itemsInCart.filter(item => item !== foundItem)
        } else {
          itemsInCart = [...itemsInCart, item]
        }
      }

      return ({
        ...state,
        itemsInCart
      })
    }
    case types.CLEAR_CART: {
      return ({
        ...state,
        itemsInCart: []
      })
    }
    default: {
      return state;
    }
  }
}