import React from 'react';
import * as types from "../constants/types"

const initState = {
  isModalOpen: false,
  currentItem: null
}

export default function modalStateReducer(state = initState, action = {}) {
  switch (action.type) {
    case types.TOGGLE_MODAL: {

      return ({
        ...state,
        isModalOpen: !state.isModalOpen,
        currentItem: action.payload
      })
    }
    default: {
      return state;
    }
  }
}