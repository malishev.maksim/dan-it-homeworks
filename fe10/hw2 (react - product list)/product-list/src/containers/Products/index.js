import React, { Component, useState, useEffect } from "react"
import List from "../../components/List/List"
import { connect, useSelector, useDispatch } from "react-redux"
import {setItems} from "../../actions/itemsActions"
import { bindActionCreators } from "redux";

export default function Products(props) {

  const items = useSelector(state=> state.itemReducer.items);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setItems());
  }, []);  

  return (
    <div>
      <h1>Products:</h1>
      <List
        items={items}
      />
    </div>
  )
}


// function mapStateToProps(store) {
//   return {
//     items: store.itemReducer.items
//   }
// }

// function mapDispatchToProps(dispatch) {
//   return {
//     setItems: bindActionCreators(setItems, dispatch)
//   }
// }

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(Products);








// export default class Products extends Component {

//   constructor(props) {
//     super(props);
//     this.state = {
//       items: []
//     }
//   }

//   componentDidMount() {
//     getItems("./items.json")
//       .then((items) => {
//         this.setState({
//           items
//         })
//       });
//   }

//   render() {
//     return (
//       <div>
//         <h1>Products:</h1>
//         <List
//           items={this.state.items}

//         />
//       </div>
//     )
//   }
// }