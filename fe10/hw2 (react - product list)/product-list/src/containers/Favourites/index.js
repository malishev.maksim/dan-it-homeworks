import React, { Component, useState, useEffect } from "react"
import List from "../../components/List/List"
import { connect, useSelector, useDispatch } from "react-redux"
import { bindActionCreators } from "redux";

export default function Favourites() {

  const favouriteItems = useSelector(state => state.favouritesReducer.favouriteItems)
  const dispatch = useDispatch();

  return (
    <div>
      <h1>Favourites:</h1>
      <List
        items={favouriteItems}
      />
    </div>
  )
}

// function mapStateToProps(store) {
//   return {
//     favouriteItems: store.favouritesReducer.favouriteItems
//   }
// }

// function mapDispatchToProps(dispatch) {
//   return {
//     toggleFavourites: bindActionCreators(toggleFavourites, dispatch)
//   }
// }

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(Favourites);