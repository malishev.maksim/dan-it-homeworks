import React, { useState, useEffect } from 'react';
import './item.scss';
import Modal from "../../components/Modal/Modal"
import Button from "../../components/Button/Button"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faWindowClose } from '@fortawesome/free-solid-svg-icons'
import { toggleFavourites } from '../../actions/favouritesActions';
import { toggleItemInCart } from "../../actions/itemsInCartActions";
import { toggleForm } from "../../actions/formActions"
import {toggleModal} from "../../actions/modalActions"
import { connect, useSelector, useDispatch } from "react-redux"
import { bindActionCreators } from "redux";

export default function Item(props) {

  const dispatch = useDispatch();
  const favouriteItems = useSelector(state=> state.favouritesReducer.favouriteItems);
  const itemsInCart = useSelector(state=> state.itemsInCartReducer.itemsInCart);
  const isModalOpen = useSelector(state=> state.modalStateReducer.isModalOpen);
  const currentItem = useSelector(state=> state.modalStateReducer.currentItem);
  const {showCloseButton, item} = props;
  const { name, artist, price, url } = item;

  function isItemInFavourites() {
    return favouriteItems && favouriteItems.find(thisItem => thisItem.sku === item.sku)
  }

  function isItemInCart() {
    return itemsInCart && itemsInCart.find(thisItem => thisItem.sku === item.sku)
  }

  return (
    <>
      <div className="item">
        <img className="item__img" src={url}>
        </img>
        {showCloseButton && <button
          className="item__close-icon"
          onClick={() => dispatch(toggleItemInCart(item))}
        >
          <FontAwesomeIcon icon={faWindowClose} />
        </button>}
        <div className="item__description">
          <button
            className={isItemInFavourites() ? "item-favourite-icon item-is-favourite" : "item-favourite-icon"}
            onClick={() => {
              dispatch(toggleFavourites(item))
            }}
          >
            <FontAwesomeIcon icon={faStar} />
          </button>
          <div className="item__song-title">
            <h2 className="item__song-name">{name}</h2>
            <span className="item__song-artist">{artist}</span>
          </div>
          <p className="item__song-description">Lorem lore ai li li</p>
          <div className="item__footer">
            <span className="item__song-price">{price}</span>
            <button
              className="item__action-button"
              onClick={isItemInCart() ? () => { } : () => dispatch(toggleModal(item))}
            >
              {isItemInCart() ? "In cart" : "Add to cart"}
            </button>
          </div>
        </div>
      </div>
      {
        isModalOpen && currentItem === item && <Modal
          isCloseButton={true}
          headerText="Add this song to card?"
          mainText={`Add ${name} to cart?`}
          actionButtons={[
            <Button
              key="Cancel"
              text="Cancel"
              onClick={() => dispatch(toggleModal())}
            />,
            <Button
              key="Add"
              text="Add"
              onClick={() => {
                dispatch(toggleItemInCart(item));
                dispatch(toggleModal())
              }}
            />
          ]}
        />
      }
    </>
  )
}



// function mapStateToProps(store) {
//   return {
//     favouriteItems: store.favouritesReducer.favouriteItems,
//     itemsInCart: store.itemsInCartReducer.itemsInCart
//   }
// }

// function mapDispatchToProps(dispatch) {
//   return {
//     toggleFavourites: bindActionCreators(toggleFavourites, dispatch),
//     toggleItemInCart: bindActionCreators(toggleItemInCart, dispatch),
//   }
// }

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(Item);
