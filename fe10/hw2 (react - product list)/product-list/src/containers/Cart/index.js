import React, { Component, useState, useEffect, useLo } from "react"
import List from "../../components/List/List"
import { connect, useSelector, useDispatch } from "react-redux"
import {useLocation} from "react-router-dom"
import { bindActionCreators } from "redux";
import {toggleModal} from "../../actions/modalActions"
import {toggleForm} from "../../actions/formActions"
import {clearCart} from "../../actions/itemsInCartActions"
import CustomerInfoForm from "../../components/CustomerInfoForm/form"


export default function Cart() {

  const itemsInCart =  useSelector(state=> state.itemsInCartReducer.itemsInCart);
  const isCursomerFormOpen =  useSelector(state=> state.showFormReducer.isCustomerFormOpen);
  const dispatch = useDispatch();

  return (
    <div>
      <h1>Cart:</h1>
      <List
        items={itemsInCart}
        showCloseButton={true}
      />
      {isCursomerFormOpen &&
      <CustomerInfoForm
        onSubmit = {(values) => {
          console.table(values);
          dispatch(toggleForm());
          dispatch(clearCart());
        }}
      ></CustomerInfoForm>}
    </div>
  )
}

// function mapStateToProps(store) {
//   return {
//     itemsInCart: store.itemsInCartReducer.itemsInCart
//   }
// }

// function mapDispatchToProps(dispatch) {
//   return {
//     toggleItemInCart: item=> dispatch(toggleItemInCart(item))
//   }
// }

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(Cart);