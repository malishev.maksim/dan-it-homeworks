// fetching items from public folder
export default async function getItems(url) {
  let itemsData = await fetch(url)
    .then(data => data.json())
    .then(products => {
      return products.products;
    });
  return itemsData;
}