import React, { useState } from 'react';
import { Switch, Route } from 'react-router-dom';
import Favourites from "./containers/Favourites"
import Cart from "./containers/Cart"
import Toolbar from './components/Toolbar';
import Products from "./containers/Products"
import './App.css';


// class App extends React.Component {

//   render() {
//     return (
// <div className="App">
//   <Toolbar />
//   <Switch>
//     <Route exact
//       path="/"
//       component={() => <Products />}
//     />
//     <Route exact
//       path="/favourites"
//       component={() => <Favourites />}
//     />
//     <Route exact
//       path="/cart"
//       component={() => <Cart />}
//     />
//   </Switch>

// </div>
//     )
//   }
// }

function App() {
  return (
    <div className="App">
      <Toolbar />
      <Switch>
        <Route exact
          path="/"
          component={() => <Products />}
        />
        <Route exact
          path="/favourites"
          component={() => <Favourites />}
        />
        <Route exact
          path="/cart"
          component={() => <Cart />}
        />
      </Switch>

    </div>
  )
}

export default App;
