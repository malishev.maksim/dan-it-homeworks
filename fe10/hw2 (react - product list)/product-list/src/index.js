import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {configureStore} from "./store"
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';

const getState = () => {
  try {
    let initialState = localStorage.getItem("redux_state");
    if (initialState === null) {
      return undefined;
    }
    return JSON.parse(initialState);
  } catch (err) {
    return undefined;
  }
} 

const setState = (state) => {
  let newState = JSON.stringify(state);
  localStorage.setItem("redux_state", newState);
}

const store = configureStore(getState());

store.subscribe(() => setState(store.getState()));

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
