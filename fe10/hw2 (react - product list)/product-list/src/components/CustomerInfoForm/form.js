import React from 'react';
import { Field, reduxForm } from 'redux-form';
import Button from '../Button/Button';
import { FormControl, FormHelperText } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { toggleForm } from "../../actions/formActions"
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import TextField from '@material-ui/core/TextField';
import {toggleModal} from "../../actions/modalActions"
import Input from '@material-ui/core/Input';
import "./form.scss"

const required = value => value ? undefined : 'Required';
const number = value => value && !isNaN(Number(value)) ? undefined : "Must be a number";
const string = value => value && !(/\p{L}+/u.test(value)) ? "Enter valid name" : undefined;

const renderTextField = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
    <TextField
      label={label}
      placeholder={label}
      margin="normal"
      error={touched && invalid}
      variant="outlined"
      helperText={touched && error}
      {...input}
      {...custom}
    />
  );

function CustomerInfoForm(props) {

  const dispatch = useDispatch();

  function closeModal(e) {
    if (!e.target.closest(".form")) {
      dispatch(toggleForm());
    }
  }

  const { handleSubmit } = props;

  return (
    <>
      <div className="backdrop" onClick={closeModal}>
        <form className="form" onSubmit={handleSubmit}>
          <h1>Your order data</h1>
          <Field
            name="name"
            label="Name"
            component={renderTextField}
            validate={[required, string]}
          />
          <Field
            name="age"
            label="Age"
            component={renderTextField}
            validate={[required, number]}
          />
          <Field
            name="address"
            label="Address"
            component={renderTextField}
            validate={required}
          />
          <Button
            type="submit"
            label="submit"
            color="white"
            backgroundColor="#0F4958"
            text="Confirm"
          >
          </Button>
        </form>
      </div>
    </>
  )
}

export default reduxForm({
  form: "customer-info"
})(CustomerInfoForm);