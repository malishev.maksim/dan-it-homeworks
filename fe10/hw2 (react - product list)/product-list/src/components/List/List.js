import React from 'react';
import Item from "../../containers/Item/Item"
import {toggleItemInCart} from "../../actions/itemsInCartActions"
import "./list.scss"
import { useDispatch } from 'react-redux';
import { toggleFavourites } from '../../actions/favouritesActions';


function List(props) {
  let items = props.items;
  const dispatch = useDispatch();

  return (
    <div>
      <div className="items-list">
        {items.map(item => <Item
          key={item.sku}
          item={item}
          showCloseButton={props.showCloseButton}
        />
        )}
      </div>
    </div>
  )
}

export default List;