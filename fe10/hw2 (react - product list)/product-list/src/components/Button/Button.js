import React from "react";
import PropTypes from 'prop-types';
import "./button.css"

export default function Button(props) {
  const {
    text,
    backgroundColor,
    color,
    onClick
  } = props;

  return (
    <button
        className = "button"
        style={{backgroundColor: backgroundColor, color: color}}
        onClick={onClick}
      >
        {text}
      </button>
  )
}

// export default class Button extends React.Component {

//   render() {
//     const {
//       text,
//       color,
//       onClick
//     } = this.props;
    
//     return (
//       <button
//         className = "button"
//         style={{ backgroundColor: color }}
//         onClick={onClick}
//       >
//         {text}
//       </button>
//     )
//   }
// }

Button.propTypes = {
  text: PropTypes.string,
  color: PropTypes.string,
  onClick: PropTypes.func
};