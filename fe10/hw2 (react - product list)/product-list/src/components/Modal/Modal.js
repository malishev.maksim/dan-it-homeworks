import React from "react";
import PropTypes from 'prop-types';
import "./modal.css"
import { useDispatch} from "react-redux"
import {toggleModal} from "../../actions/modalActions"

export default function Modal(props) {

  const dispatch = useDispatch();

  const { headerText,
    isCloseButton,
    mainText,
    actionButtons
  } = props;

  function closeModal(e) {
    if (!e.target.closest(".modal")) {
      dispatch(toggleModal());
    }
  }

  const closeButton = isCloseButton ?
    <button
      className="close-button"
      onClick={() => dispatch(toggleModal())}
    >X
    </button> : "";

  return (
    <div className="backdrop" onClick={closeModal}>
      <div className="modal">
        <div className="modal__header">
          <h1 className="modal__header__text">{headerText}</h1>
          {closeButton}
        </div>
        <div className="modal__body">
          <p className="modal__text">
            {mainText}
          </p>
        </div>
        <div className="modal__footer">
          {actionButtons}
        </div>
      </div>
    </div>
  )
}

// export default class Modal extends React.Component {
//   constructor(props) {
//     super(props);

//     this.closeModal = this.closeModal.bind(this);
//   }

//   closeModal(e) {
//     if (!e.target.closest(".modal")) {
//       this.props.onClose();
//     }
//   }

//   render() {
//     const { headerText,
//       isCloseButton,
//       mainText,
//       actionButtons,
//       onClose
//     } = this.props;

//     const closeButton = isCloseButton ?
//       <button
//         className="close-button"
//         onClick={onClose}
//       >X
//       </button> : "";

//     return (
//       <div className="backdrop" onClick={this.closeModal}>
//         <div className="modal">
//           <div className="modal__header">
//             <h1 className="modal__header__text">{headerText}</h1>
//             {closeButton}
//           </div>
//           <div className="modal__body">
//             <p className="modal__text">
//               {mainText}
//             </p>
//           </div>
//           <div className="modal__footer">
//             {actionButtons}
//           </div>
//         </div>
//       </div>
//     )
//   }
// }

Modal.propTypes = {
  headerText: PropTypes.string,
  isCloseButton: PropTypes.bool,
  mainText: PropTypes.string,
  actionButtons: PropTypes.array,
  closeModal: PropTypes.func
};