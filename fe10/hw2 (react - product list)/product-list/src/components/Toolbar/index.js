import React from "react"
import { Link } from "react-router-dom"
import Button from "../Button/Button"
import { useLocation } from "react-router-dom"
import "./toolbar.scss"
import { useDispatch } from "react-redux"
import { toggleForm } from "../../actions/formActions"


export default function Toolbar(props) {

  const currentPath = useLocation().pathname;
  const dispatch = useDispatch();

  return (
    <div className="top-nav-wrapper">
      <ul className="top-nav">
        <li ><Link className="top-nav__link" to="/">Products</Link></li>
        <li ><Link className="top-nav__link" to="/favourites">Favourites</Link></li>
        <li ><Link className="top-nav__link" to="/cart">Cart</Link></li>
      </ul>
      <div className="toolbar-buttons">
        {currentPath === "/cart" && <Button
          key="Checkout"
          text="Checkout"
          backgroundColor={"#C1F4FF"}
          color={"black"}
          onClick={() => {
            dispatch(toggleForm())
          }}
        ></Button>}
      </div>

    </div>
  )
}

// export default class Toolbar extends React.Component {
//   render() {
//     return (
//       <div className="top-nav-wrapper">
//         <ul className="top-nav">
//           <li ><Link className="top-nav__link" to="/">Products</Link></li>
//           <li ><Link className="top-nav__link" to="/favourites">Favourites</Link></li>
//           <li ><Link className="top-nav__link" to="/cart">Cart</Link></li>
//         </ul>
//       </div>
//     )
//   }
// }