import React from 'react';
import * as types from "../constants/types"

export function toggleFavourites(item) {

  return dispatch => dispatch({
    type: types.TOGGLE_FAVOURITE,
    payload: item
  })
}