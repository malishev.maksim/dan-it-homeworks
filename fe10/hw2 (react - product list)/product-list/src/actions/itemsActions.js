import React from 'react';
import getItems from "../services/tasks"
import * as types from "../constants/types"

export function setItems() {  

  return async dispatch => {
    const items = await getItems("./items.json");

    dispatch({
      type: types.ADD_ITEMS,
      payload: items
    })
  }
}