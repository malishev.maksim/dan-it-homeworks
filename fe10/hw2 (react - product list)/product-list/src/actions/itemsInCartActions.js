import React from 'react';
import * as types from "../constants/types"

export function toggleItemInCart(item) {
  return dispatch=>dispatch({
    type: types.TOGGLE_ITEM_IN_CART,
    payload: item
  })
}

export function clearCart() {
  return dispatch=> dispatch({
    type: types.CLEAR_CART
  })
}