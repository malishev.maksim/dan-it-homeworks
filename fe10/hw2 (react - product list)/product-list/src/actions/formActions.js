import React from 'react';
import * as types from "../constants/types"

export function toggleForm() {  

  return dispatch=> dispatch({
    type: types.TOGGLE_FORM
  })
}