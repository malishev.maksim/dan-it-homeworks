import React from 'react';
import * as types from "../constants/types"

export function toggleModal(item) {
  return dispatch=>dispatch({
    type: types.TOGGLE_MODAL,
    payload: item
  })
}