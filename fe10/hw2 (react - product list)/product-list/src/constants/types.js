export const ADD_ITEMS = "ADD_ITEMS";
export const TOGGLE_FAVOURITE = "TOGGLE_FAVOURITE";
export const TOGGLE_ITEM_IN_CART = "TOGGLE_ITEM_IN_CART";
export const TOGGLE_MODAL = "TOGGLE_MODAL";
export const TOGGLE_FORM = "TOGGLE_FORM";
export const CLEAR_CART = "CLEAR_CART";