let promise = fetch("https://swapi.co/api/films/");
promise
  .then(response => response.json())
  .then(result => {
    console.log(result.results);
    let films = result.results;
    for (let film of films) {
      let filmDiv = document.createElement("div");
      filmDiv.classList.add("film");
      let episodeNumber = document.createElement("p");
      episodeNumber.classList.add("episode-number");
      episodeNumber.innerText = "Episode " + film.episode_id;
      filmDiv.append(episodeNumber);
      episodeNumber.insertAdjacentHTML("afterend", `${film.title} <br> ${film.opening_crawl} <br><br>`);
      document.querySelector(".wrapper").append(filmDiv);

      let namePromises = [];
      let charactersURLs = film.characters;

      for (let characterURL of charactersURLs) {
        //делаем промис не всего объекта по ссылке, а конкретно промис имени актера
        let namePromise = fetch (characterURL).then(response => response.json()).then(character => character.name);
        console.log(namePromise);
        namePromises.push(namePromise)
      }

      Promise.all(namePromises).then (
        function (names) {
          console.log(names);
          let charactersDiv = document.createElement("div");
          charactersDiv.innerHTML = `Characters: ${names}`;
          filmDiv.append(charactersDiv);
        }
      )
    }
  })
