const xhr = new XMLHttpRequest();
xhr.open("GET", "https://swapi.co/api/films/");
xhr.send();

xhr.onload = function () {
  if (this.status === 200) {
    const films = JSON.parse(this.responseText).results;
    console.log(films);

    for (let film of films) {
      let filmDiv = document.createElement("div");
      filmDiv.classList.add("film");
      filmDiv.innerHTML = `Episode ${film.episode_id} <br> ${film.title} <br> ${film.opening_crawl} <br><br>`;
      document.body.append(filmDiv);

      //Получение персонажей
      //Имя каждого персонажа - это отдельный запрос. Мне нужно вывести имя всех персонажей, как только все персонажи по фильму получены. Поэтому нужно испльзовать Promise.all(), в который нужно передать массив промисов (в кажом из которых будет получаться отдельное имя по запросу)
      let characterNamesPromises = [];
      let charactersURLs = film.characters;

      for (let characterURL of charactersURLs) {
        let characterNamePromise = new Promise((resolve, reject) => {
          let xhr = new XMLHttpRequest();
          xhr.open("GET", characterURL);
          xhr.send();

          xhr.onload = function () {
            if (this.status === 200) {
              //в качестве результата промиса возвращаем конкретное имя
              return resolve(JSON.parse(this.responseText).name);
            }
          }
        })
        characterNamesPromises.push(characterNamePromise);
      }
      //как только ВСЕ промисы завершатся, сделай своим результатом массив результатов каждого отдельного промиса (т.е. в итоге будет массив имен)
      let characterNames = Promise.all(characterNamesPromises);
      characterNames.then(
        function (names) {
          let charactersDiv = document.createElement("div");
          charactersDiv.innerHTML = `Characters: ${names}`;
          filmDiv.append(charactersDiv);
        }
      )
    }
  }
}