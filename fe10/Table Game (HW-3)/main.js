class Game {

  constructor(difficulty, player1, player2) {
    this.difficulty = difficulty;
    this.player1 = player1;
    this.player2 = player2;
    console.log(this.player1, this.player2);
  }

  checkWinner() {
    if (this.player1.score > this.player2.score) {
      this.winner = this.player1;
    } else this.winner = this.player2;
  }

  outputWinner() {
    let winnerMessage = document.querySelector(".winnerMessage");
    winnerMessage.classList.remove("hidden");
    let winnerText = document.querySelector(".winnerMessage h1");
    if (this.winner === this.player1) {
      winnerText.innerHTML = `${this.player1.name} is winner!`;
    } else {
      winnerText.innerHTML = `${this.player1.name}, you lose &#128565;`;
    }

  }

  isPointsToWinAcheaved(pointsToWin) {
    return (this.player1.score >= pointsToWin || this.player2.score >= pointsToWin);
  }

  play() {
    //CellsArray - is array from wich we will chose random cell
    //It will be recuced each step, because no cell must be active twice
    let cellsArray = Array.from(document.querySelectorAll("td"));

    processCell.call(this);

    function processCell() {
      //Select and style random cell
      let randomCell = selectRandomCell();
      randomCell.classList.add("active");
      let randomCellClicked = false;

      //Verify if player hit the random cell at time
      randomCell.addEventListener("mousedown", checkPointForPlayer1.bind(this));

      //If cell is not clicked for cellActiveTime we count point for Player2 (AI)
      //and style active cell properly
      setTimeout(() => {
        randomCell.classList.remove("active");

        if (!randomCellClicked) countPointForPlayer2.call(this);

        //inside setTimeout we verify end game condition
        //If not end then we repeat the step (all logic off processing active cell)
        if (this.isPointsToWinAcheaved(5)) {
          this.checkWinner();
          this.outputWinner();
        } else processCell.call(this);
      }, this.difficulty.speed);

      function selectRandomCell() {
        let randomCell = cellsArray[Math.floor(Math.random() * cellsArray.length)];
        cellsArray.splice(cellsArray.indexOf(randomCell), 1);
        return randomCell;
      }

      function checkPointForPlayer1(event) {
        if (event.target.classList.contains("active")) {
          this.player1.score++;
          randomCell.classList.add("hit");
          randomCellClicked = true;
        }
      };

      function countPointForPlayer2() {
        this.player2.score++;
        randomCell.classList.add("miss");
        console.log(this.player1.score, this.player2.score);
      }
    }
  }
}

//Define list of difficulties

const HARD = {
  speed: 500
};

const EASY = {
  speed: 1500
};

const MEDIUM = {
  speed: 1000
};

class Player {
  constructor(name) {
    this.name = name;
    this.score = 0;
  }
};

//Start game logic (processing user input)
let options = document.getElementById("difficulty");

let playBtn = document.querySelector("#play");
playBtn.addEventListener("mousedown", () => {
  let difficulty = checkSelectedDifficulty(options.value);
  let player1 = new Player(document.querySelector("#name").value);
  console.log(player1);
  //Player2 is computer by default
  let player2 = new Player("Ai");
  let game = new Game(difficulty, player1, player2);
  game.play();
})

function checkSelectedDifficulty(value) {
  switch (value) {
    case "EASY": {
      return EASY;
    }
    case "HARD": {
      return HARD;
    }
    case "MEDIUM": {
      return MEDIUM;
    }
  }
}

let playAgainBtn = document.querySelector("#play-again");
playAgainBtn.addEventListener("mousedown", () => {
  location.reload();
})
