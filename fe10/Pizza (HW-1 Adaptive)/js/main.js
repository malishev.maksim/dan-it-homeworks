document.querySelector(".open-nav-menu-btn").addEventListener("click", ()=> {
  toggleClass(
    "hidden",
    document.querySelector(".open-nav-menu-btn"),
    document.querySelector(".close-nav-menu-btn"),
    document.querySelector(".nav-item-list"),
  )
});

document.querySelector(".close-nav-menu-btn").addEventListener("click", ()=> {
  toggleClass(
    "hidden",
    document.querySelector(".open-nav-menu-btn"),
    document.querySelector(".close-nav-menu-btn"),
    document.querySelector(".nav-item-list"),
  )
});

function toggleClass(classToToggle, ...rest) {
  console.log(rest);
  for (let el of rest) {
    el.classList.toggle(classToToggle)
  }
}