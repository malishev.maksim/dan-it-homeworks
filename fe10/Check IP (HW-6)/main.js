const checkIPButton = document.querySelector("#check-ip");
checkIPButton.addEventListener("click", async () => {
  let response = await fetch ('https://api.ipify.org/?format=json');//код остановится на этой строке, но это не блокирует JS движок
  let ip;

  if (response.ok) {
    const data = await response.json();//код остановится на этой строке, но это не блокирует JS движок
    ip = data.ip;
    console.log(ip);
  }

  let response2 = await fetch(`http://ip-api.com/json/${ip}?lang=ru&fields=country,city,zip`);//код остановится на этой строке, но это не блокирует JS движок
  if (response2.ok) {
    const data = await response2.json();//код остановится на этой строке, но это не блокирует JS движок
    console.log(data);
    const locationData = `${data.country} ${data.city} ${data.zip}`;

    const outputElem = document.createElement("p");
    outputElem.append(locationData);
    document.querySelector("button").after(outputElem);
  }
})