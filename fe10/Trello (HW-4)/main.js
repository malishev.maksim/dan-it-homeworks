class Column {
  constructor(columnTitle) {
    this.title = columnTitle;
    this.cards = [];
  }

  render() {
    //generating column and its buttons
    const column = document.createElement("div");
    column.classList.add("column");

    const title = document.createElement("h2");
    title.classList.add("column-title");
    title.innerText = this.title;
    column.append(title);

    const sortButton = document.createElement("button");
    sortButton.classList.add("sort-cards-btn");
    sortButton.innerHTML = `<i class="fa fa-sort-alpha-asc" aria-hidden="true"></i>`;
    column.prepend(sortButton);

    const cardsContainer = document.createElement("div");
    cardsContainer.classList.add("cards-container");
    column.append(cardsContainer);

    const addButton = document.createElement("button");
    addButton.classList.add("add-card-btn");
    addButton.innerText = "+ Add card";
    column.append(addButton);

    document.body.append(column);
    //Start listening click on Add card button
    addButton.addEventListener("click", (e) => {
      this.addCard(e.target.closest(".column").querySelector(".cards-container"));
    })

    //Start listening click on Sort cards button
    sortButton.addEventListener("click", (e) => {
      this.sortCards(e.target.closest(".column"));
    })

    //card drag&drop logic
    column.addEventListener("mousedown", (e) => {
      const currentCard = e.target;
      if (currentCard.classList.contains("card")) {
        document.body.style.cursor = "move";

        currentCard.style.position = "absolute";
        currentCard.style.zIndex = 1000;
        //смещение на момент клика = Y мыши отностиельно окна - ( Y верха Карточки - Y верха Колонки )
        let shiftY = e.clientY - (currentCard.getBoundingClientRect().top - document.querySelector(".column").getBoundingClientRect().top);

        document.addEventListener("mousemove", moveCard);
        function moveCard(e) {
          currentCard.style.top = e.clientY - shiftY + "px";
        }

        document.addEventListener("mouseup", (e) => {
          document.removeEventListener("mousemove", moveCard);
          const cards = Array.from(e.target.closest(".column").querySelectorAll(".card"));
          if (cards.every(card => card.getBoundingClientRect().top > parseInt(currentCard.style.top))) {
            e.target.closest(".column").querySelector(".cards-container").prepend(currentCard);
          } else {
            for (let card of cards) {
              let cardRect = card.getBoundingClientRect();
              if (parseInt(currentCard.style.top) > cardRect.top) {
                card.after(currentCard);
              }
            }
          }
          //clear all styles of card to default for normal flow
          currentCard.style.position = "static";
          currentCard.style.top = "";

          document.body.style.cursor = "initial";
        })
      };
    })
  }

  addCard(container) {
    let newCard = new Card;
    newCard.render(container);
  }

  sortCards(column) {
    const cards = Array.from(column.querySelectorAll(".card"));
    //sort cards by text content in 'textarea' element (it is first child in card)
    cards.sort(function (a, b) {
      let firstText = a.firstElementChild.value.toUpperCase();
      let secondText = b.firstElementChild.value.toUpperCase();
      if (firstText < secondText) {
        return -1;
      } else {
        return 1;
      }
    });
    for (let card of cards) {
      column.querySelector(".cards-container").append(card);
    }
  }
}

class Card {
  constructor(text = this.randomWord(), color = this.randomColor()) {
    this.text = text;
    this.color = color;
  }
  randomWord() {
    const words = ["Sleep", "Run ", "Nap", "Eat", "Scream"];
    return words[Math.floor(Math.random() * words.length)];
  }
  randomColor() {
    return `RGB(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`
  }
  render(container) {
    const card = document.createElement("div");
    card.classList.add("card");
    card.insertAdjacentHTML("afterbegin", `<textarea name="card-text" id="card-text" >${this.text}</textarea>`);
    card.style.backgroundColor = this.color;
    container.prepend(card);
  }
}

//testing

let column1 = new Column("To do");
column1.render();
let column2 = new Column("Next");
column2.render();