
class Hamburger {
  constructor(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this._toppings = []; //правильно ли вот так создать типа защищенный массив. Или надо тоже как-то через сеттер?

    size && stuffing && size.sizeName && stuffing.stuffing && console.log(size.sizeName + " hamburger with " + this.stuffing.stuffing + " stuffing prepared!");
  }

  get size() {
    return this._size
  }
  set size(value) {
    try { //Вот этот try лушче делать внутри сеттера, или сеттер оборачивать в try catch?
      if (!value) throw new HamburgerException("Size required");
      if (value.type != "size") throw new HamburgerException("Wrong size");
      this._size = value;
    } catch (err) {
      console.error(err.message);
    }
  }

  get stuffing() {
    return this._stuffing;
  }
  set stuffing(value) {
    try { //Вот этот try лушче делать внутри сеттера, или сеттер оборачивать в try catch?
      if (!value) throw new HamburgerException("Stuffing required");
      if (value.type != "stuffing") throw new HamburgerException("Wrong stuffing");
      this._stuffing = value;
    } catch (err) {
      console.error(err.message);
    }
  }

  get toppings() {
    return this._toppings;
  }

  addTopping(topping) {
    try {
      if (!topping) {
        throw new HamburgerException("Topping required!");
      } else if (this._toppings.includes(topping)) {
        throw new HamburgerException("Duplicate topping");
      }
      this._toppings.push(topping);
      topping && console.log(topping.toppingName + " topping added");
    } catch (err) {
      console.error(err.message);
    }
  }

  removeTopping(topping) {
    try {
      if (!topping) {
        throw new HamburgerException("Specify topping to remove!");
      } else if (!this._toppings.includes(topping)) {
        throw new HamburgerException("No such topping in burger!");
      }
      this._toppings.splice(this.toppings.indexOf(topping), 1);
      topping && console.log(topping.toppingName + " topping removed");
    } catch (err) {
      console.error(err.message);
    }
  }

  getSizeName() {
    return this._size.sizeName;
  }

  get stuffing() {
    return this._stuffing;
  }

  calculatePrice() {
    let totalPrice = this._size.price + this._stuffing.price;
    for (let topping of this._toppings) {
      totalPrice += topping.price;
    }
    return totalPrice;
  }

  calculateCalories() {
    let totalCalories = this._size.cal + this._stuffing.cal;
    for (let topping of this._toppings) {
      totalCalories += topping.cal;
    }
    return totalCalories;
  }
}

//Constant sizes, toppings...
Hamburger.SIZE_SMALL = {
  type: "size",
  sizeName: "Small",
  price: 50,
  cal: 20
};
Hamburger.SIZE_LARGE = {
  type: "size",
  sizeName: "Large",
  price: 100,
  cal: 40
};
Hamburger.STUFFING_CHEESE = {
  type: "stuffing",
  stuffing: "cheese",
  price: 10,
  cal: 20
};
Hamburger.STUFFING_SALAD = {
  type: "stuffing",
  stuffing: "salad",
  price: 20,
  cal: 5
};
Hamburger.STUFFING_POTATO = {
  type: "stuffing",
  stuffing: "potato",
  price: 15,
  cal: 10
};
Hamburger.TOPPING_MAYO = {
  type: "topping",
  toppingName: "mayo",
  price: 20,
  cal: 5
};
Hamburger.TOPPING_SPICE = {
  type: "topping",
  toppingName: "spice",
  price: 15,
  cal: 0
};;

//Custom UserException constructor
function HamburgerException(message) {
  this.name = "hamburgerException";
  this.message = message;
}

//POSITIVE CASES:
let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamburger);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.size === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.toppings.length); // 1

//NEGATIVE CASES:
console.log("-->trying to pass wrong parameters to Hamburger constructor:");
// не передали обязательные параметры
var h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'




//ADDITIONAL LOGIC FOR USER INTERACTION (NOT COMPLETE!)
let size, stuffing, topping, burger;

const choises = document.querySelectorAll(".choise");
for (let choise of choises) {
  choise.addEventListener("click", makeChoise);
}

function makeChoise(e) {
  e.target.classList.toggle("chosen");
  switch (e.target.dataset.choise) {
    case "big": {
      size = Hamburger.SIZE_LARGE;
      break;
    }
    case "small": {
      size = Hamburger.SIZE_SMALL;
      break;
    }
    case "cheese": {
      stuffing = Hamburger.STUFFING_CHEESE;
      break;
    }
    case "salad": {
      stuffing = Hamburger.STUFFING_SALAD;
      break;
    }
    case "potato": {
      stuffing = Hamburger.STUFFING_POTATO;
      break;
    }
    case "mayo": {
      burger.addTopping(Hamburger.TOPPING_MAYO);
      outputBurgerInfo();
      break;
    }
    case "spice": {
      burger.addTopping(Hamburger.TOPPING_SPICE);
      outputBurgerInfo();
      break;
    }
  }
}

const makeBurgerBtn = document.querySelector(".make-burger");
makeBurgerBtn.addEventListener("click", () => {
  burger = new Hamburger(size, stuffing);
  outputBurgerInfo();
});

function outputBurgerInfo() {
  document.querySelector(".output-burger").innerHTML = burger.size.sizeName + " burger with " + burger.stuffing.stuffing + " prepared!" + "<br>" + "Calories: " + burger.calculateCalories() + "\n" + "Price:" + burger.calculatePrice();
}
