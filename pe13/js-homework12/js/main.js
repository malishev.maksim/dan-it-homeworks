const imgs = Array.from(document.querySelectorAll(".image-to-show"));
let playState;

let indexOfCurrentImg = 1;
let loopImgs = () => {
  for (el of imgs) el.classList.remove("visible");
  imgs[indexOfCurrentImg].classList.add("visible");
  indexOfCurrentImg++;
  if (indexOfCurrentImg === imgs.length) indexOfCurrentImg=0;
  playState = "playing";
}

let timerID = setInterval(loopImgs, 2000);

const pauseButton = document.createElement("button");
pauseButton.append("|| Pause");
pauseButton.classList.add("button");
document.body.append(pauseButton);

pauseButton.addEventListener("click", pause);
function pause (e) {
  clearInterval(timerID);
  console.log(timerID);
  playState = "paused";
}

const playButton = document.createElement("button");
playButton.append("> Play");
playButton.classList.add("button");
document.body.append(playButton);

playButton.addEventListener("click", play);
function play(e) { 
  if (playState === "paused") timerID = setInterval(loopImgs, 2000);
}