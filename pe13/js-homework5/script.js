//Cunstructor for user
function createNewUser () {

  let firstName = prompt("Enter your first name");
  let lastName = prompt("Enter you last name");
  let birthdayStr = prompt("Enter your birthday dd.mm.yyyy:");
  //Converting to string in kinda ISO format for proper parsing
  birthdayStr = birthdayStr.split(".");
  birthdayStr = birthdayStr[2] + "-" 
               + birthdayStr[1] + "-"
               + birthdayStr[0];
  birthdayStr = birthdayStr.reverse();
  let birthdayDate = new Date(birthdayStr);  

  let newUser = {
    firstName,
    lastName,
    birthdayDate
  }

  Object.defineProperty (newUser, "firstName", {
    writable: false
  });
  Object.defineProperty (newUser, "lastName", {
    writable: false
  });

  //Defining methods
  newUser.getLogin = () => {
    return (firstName[0] + lastName).toLowerCase();
  }

  newUser.setFirstName = function () {
    let name = prompt("Enter NEW first name:");
    Object.defineProperty(newUser, "firstName", {
      value: name,
    })
  }

  newUser.getAge = () => {
    let currentDate = new Date();
    let age = currentDate.getFullYear() - newUser.birthdayDate.getFullYear();

    //If current month and day is less than your birthday month and day than age will be incorrect. Fix it:
    if (currentDate.getMonth() - newUser.birthdayDate.getMonth() < 0 ||
        currentDate.getMonth() - newUser.birthdayDate.getMonth() === 0 && 
        currentDate.getDate() - newUser.birthdayDate.getDate() < 0) age --;
        
    return age;
  }

  newUser.getPassword = () => {
    return newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthdayDate.getFullYear();
  }

  return newUser;
}

//Create instanse of user
let user = createNewUser();

//Output results
console.log("Created new user form fabric:");
console.log(user);
alert("Now you will try to set new name via setter. Results will be in console");
user.setFirstName();
console.log("This user with different name, that was set though setter");
console.log(user);
console.log("This user age is " + user.getAge() );
console.log("This user password is " + user.getPassword() );