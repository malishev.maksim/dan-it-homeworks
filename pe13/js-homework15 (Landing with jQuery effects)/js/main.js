//Create navigation menu and fill it with items
//Taking text of items from section titles
$("header").before("<ul class='section-nav'></ul>");
let i = 0;
$("section").each(() => {
  let sectionTitle = $(`h2:eq(${i})`).text();
  $(".section-nav")
    .append(`<li><a class='section-nav-item'>${sectionTitle}</a></li>`);
  i++;
});

//Insert hrefs for navigation items
//Takig hrefs from "a" element in each section
let j = 0;
$(".section-nav-item").each(() => {
  let sectionLinkHref = "#" + $(`section:eq(${j}) a`).attr("name");
  $(`.section-nav-item:eq(${j})`).attr("href", sectionLinkHref);
  j++;
});

//animate scroll
$(".section-nav-item").on("click", function (e) {
  //I need to find element to scroll to by matching hrefs
  let href = $(e.target).attr("href");
  href = href.slice(1);
  let elementToScroll = $(`section a[name=${href}]`);
  //animate scrolling from current position to top offset of section
  $('.parallax-wrapper').stop().animate({
    scrollTop: $(elementToScroll).offset().top
  }, 1000);
});

//Why my scrolTop is always 0? Cant add button.
$(".parallax-wrapper").scroll(function () {
  console.log(document.querySelector('.parallax-wrapper').scrollTop);
  if (document.querySelector(".parallax-wrapper").scrollTop > 500) {
    $(".up-button").show()
  } else {
    $(".up-button").hide();
  }
})

//Go to top on click
$(".up-button").click(() => {
  $('.parallax-wrapper').stop().animate({
    scrollTop: 0,
  }, 1000);
  //Мгновенная прокрутка наверх
  //document.querySelector(".parallax-wrapper").scrollTop = "0";
})

//Slide toggle section
$(".slide-toggle-button").click(() => {
  $(".latest-news-section").slideToggle("slow");
})