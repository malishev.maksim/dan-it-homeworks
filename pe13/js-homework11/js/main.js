let btns = document.querySelectorAll(".btn");
document.body.addEventListener("keydown", onKeydown);
function onKeydown(event) {
  btns.forEach((el) => {
    if (el.innerText.toLowerCase() === event.key.toLowerCase()) {
      clearButtonsStyle(btns);
      el.style.backgroundColor = "blue";
    }
  })
  function clearButtonsStyle(nodeList) {
    nodeList.forEach((el) => {
      el.style.backgroundColor = "";
    }
    )
  }
}