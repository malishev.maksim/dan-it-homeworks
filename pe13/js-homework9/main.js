//Повесь обработчик по клику на контейнер со всеми titles
let tabTitlesContainer = document.querySelector(".tabs");
tabTitlesContainer.addEventListener("click", changeTabStyle);
//Запусти функцию отображения активной вкладки (т.к. скрытие ненужных элементов определяет именно эта функция)
window.onload = showContent();

tabTitlesContainer.addEventListener("click", showContent);

//Меняй стиль кликнутого title
function changeTabStyle(event) {
  let tabsTitles = document.getElementsByClassName("tabs-title");
  for (let el of tabsTitles) {
    el.classList.remove("active");
    event.target.classList.add("active");
  }
}

function showContent() {
  //Сначала скрываем весь контент
  let tabsContents = document.querySelector(".tabs-content").children;
  for (let el of tabsContents) {
    el.style.display = "none";
  }
  //Вычисли номер title с классом active
  let numberOfActiveTab = findNumberOfActiveTab();
  //И отобрази элемент с контентом, порядковый номер котого совпадает с номером нажатого тайтла
  let tabsContentsArr = Array.from(tabsContents);
  for (let el of tabsContentsArr) {
    if (numberOfActiveTab === tabsContentsArr.indexOf(el)) {
      el.style.display = "initial";
    }
  }
}
//Вычисляет номер title с классом active
function findNumberOfActiveTab() {
  let tabsTitles = Array.from(document.getElementsByClassName("tabs-title"));
  let numberOfActiveTab;
  for (let el of tabsTitles) {
    if (el.classList.contains("active")) numberOfActiveTab = tabsTitles.indexOf(el);
  }
  return numberOfActiveTab;
}