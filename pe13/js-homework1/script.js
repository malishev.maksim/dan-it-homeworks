let name;
let age;
do {
  name = prompt ("What is your name?", "");
  if (name == null) break; //end of loop if press Cancel
} while (!name); //if name contain any string then !name = !true = false ->> end of loop

do {
  age = prompt ("How old are you?", "");
  if (age == null) break; //end of loop if press Cancel
} while (isNaN(+age)); // isNaN(+Number) === false, isNaN(+String) === true

let welcomeMessage ="Welcome, " + name + "!";
let denyMessage = "You are not allowed to visit this website";
let confirmText = "Are you sure you want to continue?";
const minAge = 18;
const maxAge = 22;

if (age < minAge) {
  alert(denyMessage);
}
else if (age>=minAge && age <=maxAge) {
  let answer = confirm (confirmText);
  if (answer) {
    alert(welcomeMessage);
  }
  else {
    alert(denyMessage);
  }
}
else (age>maxAge) {
  alert (welcomeMessage);
}