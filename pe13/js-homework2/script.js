//>>>>>>>TASK #1. OUTPUT ALL NUMBERS THAT IS DIVIDED BY 5
alert("TASK #1\nThis program will output all numbers that is devided by 5 up to your specified limit. Are you excited? :)");
let limit = getUserInput("Enter limit:");
if (limit !== null) {//dont start program if Cancel pushed it prompt
  limit = +limit;
  //Create array of numbers wich fits to condition
  let numbersFitsList = [];
  for (let currentNumber = 1; currentNumber <= limit; currentNumber++) {
    if (currentNumber % 5 === 0) {
      numbersFitsList.push(currentNumber);//Here we can console.log currentNumber, but I want to add heading before list of numbers (see next block)
    }
  }
  outputResult(numbersFitsList, `Numbers that are devided by 5 from 1 to ${limit}:`);
}

//>>>>>>>>>TASK #2. OUTPUT ALL PRIME NUMBERS
alert("TASK #2\nThis program will output all prime numbers in your range");

let m = getUserInput("From");
let n = getUserInput("To");
if (m !== null && n !== null) {//dont start program if Cancel pushed it prompt
  m = +m;
  n = +n;
  let primeNumbersList = [];
  for (let currentNumber = m; currentNumber <= n; currentNumber++) {//go from m to n
    let isPrime = true;
    for (let devider = 2; devider < currentNumber; devider++) { //verify is currentNumber is prime (devided by only 1 and itself)
      if (currentNumber % devider === 0) {
        isPrime = false;
        break;
      }
    }
    if (isPrime === true) {
      primeNumbersList.push(currentNumber);
    }
  }
  outputResult(primeNumbersList, `Prime numbers from ${m} to ${n}:`);
}


//Verify if userInput is correct number and return this number if yes
function getUserInput(messageInPrompt) {
  let userInput;
  do {
    userInput = prompt(messageInPrompt, "");
    if (isNaN(+userInput) || +userInput - Math.floor(+userInput) !== 0 || userInput === "") { //is string? || has decimal?
      alert("Enter valid hole number\n" + userInput);
      continue;
    } else break;
  } while (true);
  return userInput;
}

//Output results to console and alert
function outputResult(numbersArray, messageBeforeList) {
  let numbersListString = "";//This is for printing to alert message
  if (!numbersArray[0]) { //if array is empty than !numbersArray[0] === true
    console.log(messageBeforeList + "\nNo numbers in you range fits your condition :(");
    alert(messageBeforeList + "\nNo numbers in you range fits your condition :(");
  } else {
    console.log(messageBeforeList);
    for (i = 0; i <= numbersArray.length - 1; i++) {
      console.log(numbersArray[i]);
      numbersListString += "\n" + numbersArray[i];//make each number apear to new line in alert message
    }
    alert(messageBeforeList + "\n" + numbersListString);
  }
}

alert("Give me 100 :)");