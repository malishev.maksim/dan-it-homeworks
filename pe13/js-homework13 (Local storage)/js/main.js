let theme = () => localStorage.getItem("currentTheme");

//Apply current theme depending of current value in local storage
if (theme() === "light" || !theme()) {
  switchToTheme("light");
} else if (theme() === "dark") {
  switchToTheme("dark");
}

//Create button and swich theme on click
const changeThemeBtn = $("<button class='theme-button'>Change theme</button>");
changeThemeBtn
.appendTo("body")
.click(() => {
    console.log("click");
    if (theme() === "light" || !theme()) {
      switchToTheme("dark");
      localStorage.setItem("currentTheme", "dark");
      return;
    }
    if (theme() === "dark") {
      switchToTheme("light");
      localStorage.setItem("currentTheme", "light");
    }
  })

function switchToTheme(theme) {
  switch (theme) {
    case "light": {
      //just playing with styles
      $("body, .link, .section-title")
      .css("color", "black");
      $(".parallax-wrapper, .bottom-triangle-edge, .top-triangle-edge, .header, .what, .meet-us, .why")
      .css("background-color", "white");
      break;
    }
    case "dark": {
      //just playing with styles
      $("body, .link, .section-title")
      .css("color", "white");
      $(".parallax-wrapper, .bottom-triangle-edge, .top-triangle-edge, .header, .what, .meet-us, .why")
      .css("background-color", "black");
      break;
    }
    default: console.log("No such theme: " + theme());
  }
}