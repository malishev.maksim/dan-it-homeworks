//>>>>>>> TASK 1. Calculator

alert("TASK #1\nThis program works like a calculator.");
let userInput1, userInput2, userInput3;
do {
  userInput1 = getUserInput("number", "Enter first number");
  if (userInput1 === null) break;
  userInput2 = getUserInput("math operator", "Enter operation");
  if (userInput2 === null) break;
  userInput3 = getUserInput("number", "Enter second number");
  if (userInput3 === null) break;

    let a = +userInput1;
    let operation = userInput2;
    let b = +userInput3;
    console.log(`${a} ${operation} ${b} = ${calculate(a, b, operation)}`);
    alert(`${a} ${operation} ${b} = ${calculate(a, b, operation)}`);
} while (true);

function calculate(a, b, operation) {
  switch (operation) {
    case "+": return a + b;
    case "-": return a - b;
    case "*": return a * b;
    case "/": return a / b;
  }
}

//Verify if userInput is correct and return this number if yes
function getUserInput(validType, messageInPrompt) {
  let userInput;
  do {
    userInput = prompt(messageInPrompt, "");
    if (validType === "number") {
      if (isNaN(+userInput) || userInput === "") { //is string? || empty string?
        alert("Enter valid number\n" + userInput);
        continue;
      } else break;

    } else if (validType === "math operator") {
      if (userInput === "+" || userInput === "-" || userInput === "*" || userInput === "/" || userInput === null) {
        break;
      } else {
        alert("Enter valid math operator\n" + userInput);
        continue;
      }
    }
  } while (true);
  return userInput;
}