# 1. Описать своими словами для чего вообще нужны функции в программировании. 

## Преимущества функций:

* избежание дублирования кода
* упрощения дальнейшего изменения кода.Не нужно менять код в нескольких местах. Достаточно поменять его один раз в функции.
* функции делают код более гибким и модульным.
* не засоряют name space, т.к. переменные внутри функции - локальные, и те же названия могут быть переиспользованы вне функции при необходимости.

# 2. Описать своими словами, зачем в функцию передавать аргумент.

Чтобы функция выполняла с ним различные действия, описанные в теле функции. Аргумент может и не передаваться, если при объявлении функции для нее не были указаны параметры.