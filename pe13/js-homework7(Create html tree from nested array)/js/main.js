//This is place to insert my list
let mainTaskDiv = document.querySelector(".main-task");

showListFromArr(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'], mainTaskDiv);
clearListAfter(10);

function showListFromArr (arr, placeToInsert) {
  //create ul to insert all list items to it
  let ul = document.createElement("ul");
  ul.classList.add("mylist");
  placeToInsert.append(ul);
  //loop though every element in array and wrap it with li tag and push to new array
  let listItems = arr.map((el) => `<li>${el}</li>`);
  //in a loop insert every element of new array to ul
  for (el of listItems) {
    ul.insertAdjacentHTML("beforeend", el);
  }
}

function clearListAfter(secondsToAction) {
  setTimeout( () => {
    let ul = document.querySelector(".mylist");
    ul.remove();
  }, secondsToAction*1000);
  showTimer(secondsToAction);
}

function showTimer(secondsToAction) {
  let timerTitle = document.createElement("p");
  timerTitle.className = "timer";
  timerTitle.innerHTML = "List will be cleared in ";
  let placeToInsert = document.querySelector(".title");
  placeToInsert.append(timerTitle);
  let span = document.createElement("span");
  span.classList.add(".timer");  
  timerTitle.append(span);
  //каждую секунду выводить значение на единицу меньше
  let timer = setInterval (() => {
      secondsToAction--;
      span.innerHTML = secondsToAction;
      //Если досчитал до нуля, очисть таймер
      if (secondsToAction <= 0) {
        clearInterval(timer);
        timerTitle.innerHTML = "Cleared!";
      }
    }, 1000);
}

//************************* */
//This is additional task to create nested DOM tree from nested Array
//КАКОЙ МЕТОД ПОДРАЗУМЕВАЛСЯ В ЭТОМ ЗАДАНИИ? КАКИЕ ЕСТЬ ЭФФЕКТИВНЫЕ МЕТОДЫ СДЕЛАТЬ ДЕРЕВО ИЗ ВЛОЖЕННЫХ ОБЪЕКТОВ/МАССИВОВ??
let obj = ['hello', [['one', 'two'], 'two'], 'Kiev', {"one":"Some", "two": "Some2"}, 'Odessa', 'Lviv'];
let parent = document.querySelector(".additional-task");
showTreeFromObj(parent, obj);

//рекурсивная функция
function showTreeFromObj(parent, obj) {
  //на каждом вызове функци для каждого объекта/массива создаем родительский ul элемент
  //чтобы уместить в него все дочерние элементы
  let ul = document.createElement("ul");
  parent.append(ul);
  for (let key in obj) {
    //базовый случай рекурсии: если значение элемента массива/объекта - это просто строка,
    //то создаем li элемент со значением и добавляем в родительский ul
    if (typeof obj[key] === "string") {
      let li = document.createElement("li");
      li.append(obj[key]);
      ul.append(li);
    } 
    //рекурсивный случай: если значение элемента массива/обхекта не строка (а объект или массив)
    //то рисуем дерево уже для этого дочернего массива/объекта
    else showTreeFromObj(ul, obj[key]);
  }
}