let input = document.getElementById("price-input");

//Change input style on fucus/blur and clear all messages
input.addEventListener("focus", (event) => {
  event.target.classList.add("focused");
  event.target.classList.remove("invalid-input-value");
  let currentPriceChips = document.querySelector(".current-price-chips");
  if (currentPriceChips) currentPriceChips.remove();
  let errorText = document.querySelector(".error-text");
  if (errorText) errorText.remove();
});

input.addEventListener("blur", (event) => {
  event.target.classList.remove("focused");
});

input.addEventListener("blur", showCurrentPriceChips);

function showCurrentPriceChips(event) {
  //Run only if there is no PriceChips yet
  //and input value is valid
  if (valid(event.target.value) && !document.querySelector(".showed-price")) {
    let parentToInsert = document.querySelector(".input-group");
    //Insert new html elements with current input value
    parentToInsert.insertAdjacentHTML("afterbegin",
      `<div class="current-price-chips">
      <span class="showed-price">Current price: ${event.target.value}</span>
      <button class="hide-price-btn">x</button>
   </div>`)
  }
};

//Set listener to parent element because there is no delete button yet and we cant
//set listener to it
let parent = document.querySelector(".input-container");
document.addEventListener("click", hidePriceChips);

function hidePriceChips(event) {
  //If event was on the right button
  if (event.target.className === "hide-price-btn") {
    //delete it along with its parent
    event.target.parentElement.remove();
    //clear input value
    let priceInput = document.querySelector(".price-input");
    priceInput.value = "";
  }
}

//validate input value
function valid(value) {
  return value >= 0 && value !== ""
}

input.addEventListener("blur", showError);

function showError(event) {
  if (!valid(event.target.value) && !document.querySelector(".error-text")) {
    document.querySelector(".price-input").classList.add("invalid-input-value");
    let errorText = document.createElement("span");
    errorText.classList.add("error-text");
    errorText.innerHTML = "Please enter correct price";
    document.querySelector(".input-group").append(errorText);
  }
}
