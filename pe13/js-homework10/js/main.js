//Set listeners on each input
let labels = document.querySelectorAll(".input-wrapper");
for (let el of labels) {
  let eyeButton = el.querySelector(".icon-password");
  let input = el.querySelector("input");
  eyeButton.addEventListener("mousedown", showPass);
  eyeButton.addEventListener("mouseup", hidePass);

  function showPass() {
    //changing icon
    eyeButton.classList.replace("fa-eye-slash", "fa-eye");
    //changing type of input to show password
    input.setAttribute("type", "text");
  }
  function hidePass() {
    //changing icon
    eyeButton.classList.replace("fa-eye", "fa-eye-slash");
    //changing type of input to hide password
    input.setAttribute("type", "password");
  }
}

let form = document.querySelector(".password-form");
//prevent reload on submit
form.addEventListener("submit", (event) => event.preventDefault());
form.addEventListener("submit", verifyPasswords);

function verifyPasswords(event) {
  let formData = new FormData(event.target);
  //show alert when password match
  if (formData.get("password-value-1") ===
    formData.get("password-value-2")) {
    alert("Welcome, user!");
    document.querySelector(".error").remove();

  }
  //show error
  else if (!document.querySelector(".error")) {
    let inputContainer = document.querySelector(".input-container");
    error = document.createElement("span");
    error.innerText = "Passwords dont match!";
    error.classList.add("error");
    inputContainer.append(error);
  }
};