//Testing
console.log("%cResult of filterBy:", "color:teal");
console.log(filterBy( ['hello', 'world', 23, '23', null],  'string')); //[23, null])

console.log("%cResult of filterBy2:", "color:teal");
console.log(filterBy2( ['hello', 'world', 23, '23', null],  'string')); //[23, null])

console.log("%cResult of filterBy3:", "color:teal");
console.log(filterBy3( ['hello', 'world', 23, '23', null],  'string')); //[23, null])

//Realization using forEach method
function filterBy(arr, type) {
  let newArr =[];
  //перебрать все элементы входного массива
  arr.forEach((el) => {
    //проверить совпадает ли их тип, и если да запушить в новый массив
    if (typeof el !== type) newArr.push(el);
  })  
  //вывести массив
  return newArr;
}

//Realization using loop for(el of Array)
function filterBy2(arr, type) {
  let newArr =[];
  for (let el of arr) {
    if (typeof el !== type) newArr.push(el);
  }  
  return newArr;
}

//Realization using filter method
function filterBy3(arr, type) {
  
  return arr.filter(el => typeof el !== type );
}