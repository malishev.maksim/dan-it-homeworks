//Styling active tab
$(".tabs li").click(function (e) {
  $(".tabs-title").removeClass("active");
  $(e.target).addClass("active");
});

$(document).ready(showContent);

$(".tabs").click(showContent);

function showContent () {
  const tabsContents = $(".tabs-content li");
  tabsContents.removeClass("active");
  let tabIndexToSwich = $(".tabs-title.active").data("tab");
  console.log(tabIndexToSwich);
  $(`.tabs-content li[data-tab = ${tabIndexToSwich}]`).addClass("active");  
}